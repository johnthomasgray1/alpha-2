from django import forms

class UserLoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(
        max_length=50,
        widget=forms.PasswordInput
    )

class UserSignupForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(
        max_length=50,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length = 50,
        widget = forms.PasswordInput,
    )

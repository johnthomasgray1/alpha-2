from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required(login_url='login')
def list_tasks(request):
    tasks = Task.objects.filter(assignee = request.user)
    context = {
        "tasks" : tasks,
    }
    return render(request, "tasks/list.html", context)

@login_required(login_url='login')
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            # Change this to redirect to the project detail page
            return redirect("list_projects")

    else:
        form = TaskForm()
    context = {
        "form" : form,
    }
    return render(request, "tasks/create.html", context)

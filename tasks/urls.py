from django.urls import path
from tasks.views import list_tasks, create_task


urlpatterns = [
    path("", list_tasks, name="list_tasks"),
    path("create/", create_task, name="create_task"),
]
